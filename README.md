# CryptoCurrencyPredictor
Research application for data visualization and prediction of cryptocurrency market values

The application consists of two services:

    - API: created with FastApi for the development of time series forecasting models
    - Web client: developed with angular for the visualization of the data

# Deploy with Docker
Commands:

    - .env file is needed to set API base url
    - docker-compose up (create and start containers)
    - docker-compose build (rebuild images with changes)
