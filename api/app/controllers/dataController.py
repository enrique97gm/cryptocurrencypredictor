from fastapi import Request
from fastapi.routing import APIRouter
import app.services.dataService as dataService
import app.models.Encoder as Encoder
import datetime

import json

router = APIRouter(prefix="/data")

@router.get("/getCurrencyList")
async def get_currency_list():
    currencyList = dataService.get_currency_list()
    return {
        "status": True,
        "data": currencyList
    }

@router.post("/getCurrencyData")
async def get_currency_data(request: Request):
    data = await request.json()
    currencyData = dataService.get_currency_data(data)
    return {
        "status": True,
        "data": currencyData
    }

@router.post("/getCurrencyValuesForecast")
async def get_currency_values_forecast(request: Request):
    data = await request.json()
    currencyList = dataService.get_currency_values_forecast(data)
    return {
        "status": True,
        "data": json.loads(json.dumps(currencyList, cls=Encoder.Encoder, ensure_ascii=False, default=default))
    }

def default(o):
    if isinstance(o, (datetime.date, datetime.datetime)):
        return o.isoformat()