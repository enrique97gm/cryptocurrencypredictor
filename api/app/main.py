from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import app.controllers.dataController as dataController

#Fast api class ini
app = FastAPI()

#Created controllers
app.include_router(dataController.router)

# Cors policy
origins = ["*"]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get("/")
def root():
    return {"status": True }
