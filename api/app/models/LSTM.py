from sklearn.preprocessing import MinMaxScaler
import time as time
import pandas as pd
import numpy as np
from tensorflow.keras.layers import LSTM, Dropout, Dense
from tensorflow.keras import Sequential
from sklearn.metrics import mean_squared_error
import json


class LSTMmodel():

    def __init__(self, params):
        self.params = params

    # Date preprocess
    def prepare_data(self, dataset):
        df = self.convert_to_datetime(dataset)
        self.df = df.copy()
        df = self.scale_data(df)
        train_data, test_data = self.split_train_test(df)
        XT, YT = self.to_sequences(
            train_data, self.params['window_size'], self.params['steps_predict'])
        xt, yt = self.to_sequences(
            test_data, self.params['window_size'], self.params['steps_predict'])
        return (XT, YT, xt, yt)

    # Data scaling
    def scale_data(self, df):
        self.scaler = MinMaxScaler(feature_range=(0, 1))
        return self.scaler.fit_transform(np.array(df).reshape(-1, 1))

    # Train test split
    def split_train_test(self, df):
        training_size = int(len(df)*0.70)
        return df[0:training_size, :], df[training_size:len(df), :]

    # To datetime and df
    def convert_to_datetime(self, dataset):
        try:
            df = pd.DataFrame(json.loads(dataset['Close']))
        except:
            df = pd.DataFrame(dataset['Close'])
        df.index = pd.to_datetime(df.index, format='%Y-%m-%d')
        return df

    # Converts data into [Previous Values] [Response Value]
    def to_sequences(self, df, window_size=1, steps_predict=1):
        X = []
        y = []
        for t in range(df.shape[0]-(window_size + steps_predict - 1)):
            X.append(df[t:(t+window_size)])
            y.append(df[(t+window_size):(t+window_size+steps_predict)])
        return(np.array(X), np.array(y))

    # Build model
    def build_model(self, X_train, y_train, X_test, y_test):
        y_train = y_train.reshape(y_train.shape[0], 1, y_train.shape[1])
        y_test = y_test.reshape(y_test.shape[0], 1, y_test.shape[1])
        model = Sequential()
        model.add(LSTM(self.params["num_neurons"][0], return_sequences=True, input_shape=(
            X_train.shape[1], X_train.shape[2])))
        model.add(Dropout(self.params["dropout"]))
        model.add(LSTM(self.params["num_neurons"][1]))
        model.add(Dropout(self.params["dropout"]))
        model.add(Dense(y_test.shape[2]))
        model.compile(optimizer="adam", loss='mean_squared_error',
                      metrics=['accuracy'])
        history = model.fit(X_train, y_train, epochs=self.params["epochs"], batch_size=self.params["batch_size"],
                            shuffle=False, verbose=0, validation_split=self.params["validation_split"])
        return ({"model": model, "history": history.history})

    def exec_model(self, dataset):
        X_train, y_train, X_test, y_test = self.prepare_data(dataset)
        modelHistory = self.build_model(X_train, y_train, X_test, y_test)
        history = modelHistory['history']
        model = modelHistory['model']
        test_predict = model.predict(X_test)
        test_predict = self.scaler.inverse_transform(test_predict)
        original_ytest = self.scaler.inverse_transform(y_test.reshape(-1, 1))
        self.df['name'] = self.df.index.view(np.int64) / int(1e6)
        print(len(original_ytest), len(test_predict))
        dfTest = self.df.iloc[-len(original_ytest):, :].copy()
        dfTestPred = self.df.iloc[-len(test_predict):, :].copy()
        self.df.rename(columns={"Close": "value"}, inplace=True)
        dfTest['value'] = original_ytest
        dfTestPred['value'] = test_predict
        graphData = [
            {
                "name": self.params['crypto'],
                "series": self.df[["name", "value"]].to_dict("records")
            },
            {
                "name": 'Test data',
                "series": dfTest[["name", "value"]].to_dict("records")
            },
            {
                "name": 'Predictions',
                "series": dfTestPred[["name", "value"]].to_dict("records")
            }
        ]

        response = {
            "params": {
                "Window size": self.params['window_size'],
                "Steps to predict": self.params['steps_predict'],
                "Epochs": self.params['epochs'],
                "Number of neurons": self.params['num_neurons'],
                "Dropout": self.params['dropout'],
                "Batch size": self.params['batch_size'],
                "Validation split": self.params["validation_split"]
            },
            "results": {
                "cvrmse": round((mean_squared_error(original_ytest, test_predict)**.5)/np.mean(original_ytest), 4),
                "rmse": round(mean_squared_error(original_ytest, test_predict)**.5, 4),
            },
            "data": graphData
        }
        if(self.params['validation_split'] != 0):
            response["losses"] = {
                "loss": history['loss'],
                "val_loss": history['val_loss']
            }

        return response
