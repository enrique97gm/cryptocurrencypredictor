import pandas_datareader as web
import datetime as dt
import pandas as pd
import numpy as np
import requests
from app.models.LSTM import LSTMmodel


def get_currency_list():
    params = {
        'start': 1,
        'limit': 100,
    }
    data = requests.get(
        'https://web-api.coinmarketcap.com/v1/cryptocurrency/listings/latest', params=params)
    data = data.json()
    curencyList = []
    for item in data['data']:
        curencyList.append({
            "name": item['name'],
            "symbol": item['symbol']
        })

    return curencyList


def get_currency_data(data):
    currency = data["currency"]
    cryptos = data["cryptos"]
    start = data["start"]
    end = data["end"]
    cryptosData = []
    for crypto in cryptos:
        df = web.DataReader(f"{crypto}-{currency}", "yahoo", start, end)
        df.rename(columns={"Close": "value", "High": "max",
                  "Low": "min"}, inplace=True)
        df["name"] = pd.to_datetime(df.index, format='%Y-%m-%d').view(np.int64) / int(1e6)
        cryptoData = {
            "name": crypto,
            "series": df[["name", "value", "max", "min"]].to_dict("records")
        }
        cryptosData.append(cryptoData)
    return cryptosData


def get_currency_values_forecast(data):
    currency = data["currency"]
    crypto = data["crypto"]
    start = data["start"]
    end = data["end"]
    params = {
        "crypto": crypto,
        "window_size": data["windowSize"],
        "steps_predict": data["stepsPredict"],
        "epochs": data["epochs"],
        "num_neurons": data["numNeurons"],
        "dropout": data["dropout"],
        "batch_size": data["batchSize"],
        "validation_split": data["validationSplit"]
    }
    cryptosData = []
    df = web.DataReader(f"{crypto}-{currency}", "yahoo", start, end)
    lstm = LSTMmodel(params)
    return lstm.exec_model(df)


    """ df.rename(columns={"Close": "value", "High": "max",
              "Low": "min"}, inplace=True)
    df["name"] = df.index.view(np.int64) / int(1e6)
    cryptoData = {
        "name": crypto,
        "series": df[["name", "value", "max", "min"]].to_dict("records")
    }
    cryptosData.append(cryptoData) """

    return cryptosData
