import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DataService } from './data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  filtersForm: FormGroup;

  data: any;
  xAxisLabel = 'Time';
  yAxisLabel = 'Value';
  cryptos = [];
  currencies = [
    {
      name: 'Euro',
      value: 'EUR'
    },
    {
      name: 'Dollar',
      value: 'USD'
    },
    {
      name: 'Pound',
      value: 'GBP'
    }
  ]

  filtersLoaded = false;
  dataLoading = false;
  dataLoaded = false;
  realTime = false;
  selectedTab = 'values';
  forecastsResult = null;

  constructor(private dataService: DataService, private readonly formBuilder: FormBuilder) { }

  ngOnInit(): void {
    const currentDate = new Date();
    this.filtersForm = this.formBuilder.group({
      currency: ['EUR'],
      selectedCryptos: [['BTC']],
      selectedCrypto: ['BTC'],
      start: [new Date(new Date().getFullYear() - 1, new Date().getMonth(), 1)],
      end: [currentDate],
      windowSize: [7],
      stepsPredict: [1],
      epochs: [25],
      lstm1: [64],
      lstm2: [32],
      dropout: [0.2],
      batchSize: [32],
      validationSplit: [0.2]
    });
    this.getCurrencyList();
    this.getCurrencyData();
  }

  getCurrencyList() {
    this.dataService.getCurrencyList().subscribe((result: any) => {
      this.cryptos = result.data;
      this.filtersLoaded = true;
    }, (error: any) => {
      console.log(error)
    });
  }

  getCurrencyData() {
    this.dataLoading = true;
    const requestData = {
      "currency": this.filtersForm.controls['currency'].value,
      "cryptos": this.filtersForm.controls['selectedCryptos'].value,
      "start": this.filtersForm.controls['start'].value,
      "end": this.filtersForm.controls['end'].value
    }
    this.dataService.getCryptoData(requestData).subscribe((result: any) => {
      this.dataLoaded = false;
      this.data = result.data;
      setTimeout(() => {
        this.dataLoaded = true;
        this.dataLoading = false;
      })
    }, (error: any) => {
      console.log(error)
      this.dataLoaded = true;
      this.dataLoading = false;
    });
  }

  getCurrencyValuesForecast() {
    this.dataLoading = true;
    const requestData = {
      "currency": this.filtersForm.controls['currency'].value,
      "crypto": this.filtersForm.controls['selectedCrypto'].value,
      "start": this.filtersForm.controls['start'].value,
      "end": this.filtersForm.controls['end'].value,
      "windowSize": this.filtersForm.controls['windowSize'].value,
      "stepsPredict": this.filtersForm.controls['stepsPredict'].value,
      "epochs": this.filtersForm.controls['epochs'].value,
      "numNeurons": [this.filtersForm.controls['lstm1'].value, this.filtersForm.controls['lstm2'].value],
      "dropout": this.filtersForm.controls['dropout'].value,
      "batchSize": this.filtersForm.controls['batchSize'].value,
      "validationSplit": this.filtersForm.controls['validationSplit'].value,
    }
    this.dataService.getCurrencyValuesForecast(requestData).subscribe((result: any) => {
      this.dataLoaded = false;
      this.data = result.data.data;
      const jsonResults = {
        "params": result.data.params,
        "results": result.data.results,
      }
      this.forecastsResult = JSON.stringify(jsonResults, undefined, 2);
      setTimeout(() => {
        this.dataLoaded = true;
        this.dataLoading = false;
      })
    }, (error: any) => {
      console.log(error)
      this.dataLoaded = true;
      this.dataLoading = false;
    });
  }

  changeFormControl(control, event) {
    this.filtersForm.controls[control].setValue(parseFloat(event.target.value));
  }

  disableCryptoOption(crypto) {
    const cryptos = this.filtersForm.controls['selectedCryptos'].value;
    return cryptos.length > 4 && !cryptos.includes(crypto.symbol);
  }

  searchCall() {
    this.forecastsResult = null;
    if (this.selectedTab === 'values') {
      this.getCurrencyData();
    } else {
      this.getCurrencyValuesForecast();
    }
  }
}
