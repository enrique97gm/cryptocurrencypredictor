import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { LineChartComponent } from '@swimlane/ngx-charts';

declare const $;

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {

  @ViewChild('chartComponent') chart: LineChartComponent;
  @Input() callbackFunction: () => void;

  @Input() data: any;
  @Input() xAxisLabel: any;
  @Input() yAxisLabel: any;
  @Input() realTime: any;

  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  showYAxisLabel = true;
  graphDataChart = [];
  colorScheme: any = {
    domain: ['#e91e63', '#1c66fa', '#5AA454', '#fc6f43', '#C7B42C']
  };
  graphData: any;

  constructor() { }

  ngOnInit(): void {
    this.graphData = [...this.data];
    if (this.realTime) {
      setInterval(() => {
        this.callbackFunction();
        this.graphData = [...this.data];
        this.chart.update();
      }, 3000);
    }
  }

  formatTimeLabels = (value: any) => {
    try {
      const options: any = { year: 'numeric', month: 'numeric', day: 'numeric' };
      return new Date(value).toLocaleString('en-GB', options).replace(",", "")
    } catch {
      return "";
    }
  }

  onSelect(event) {
    const temp = JSON.parse(JSON.stringify(this.graphData));
    if (typeof event === 'string') {
      if (this.isDataShown(event)) {
        temp.forEach(element => {
          if (element.name === event) {
            element.series = [];
          }
        });
        $('ngx-charts-legend-entry .legend-label-text').each(function () {
          if ($(this).text().trim() === event.trim()) {
            $(this).css('text-decoration', 'line-through');
            $(this).parent().css('opacity', '0.4');
          }
        })
      } else {
        const add = this.data.find(e => e.name === event);
        temp.push(add);
        temp.sort((a, b) => {
          if (a.name.toUpperCase() < b.name.toUpperCase()) {
            return -1;
          }
          return 1;
        })
        $('ngx-charts-legend-entry .legend-label-text').each(function () {
          if ($(this).text().trim() === event.trim()) {
            $(this).css('text-decoration', 'none');
            $(this).parent().css('opacity', '1');
          }
        })
      }
    }
    this.graphData = temp;
    this.chart.update();
  }

  isDataShown(event) {
    return this.graphData.find(e => e.name === event && e.series.length !== 0);
  }

}
