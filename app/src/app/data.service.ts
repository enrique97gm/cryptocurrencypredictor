import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http : HttpClient) { }

  getCurrencyList() {
    return this.http.get(environment.apiRoute + "data/getCurrencyList");
  }

  getCryptoData(requestData: any) {
    return this.http.post(environment.apiRoute + "data/getCurrencyData", requestData);
  }

  getCurrencyValuesForecast(requestData: any) {
    return this.http.post(environment.apiRoute + "data/getCurrencyValuesForecast", requestData);
  }
}
