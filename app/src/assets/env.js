(function (window) {
  window["env"] = window["env"] || {};
  // Environment variables
  window["env"]["backendBaseUrl"] = window.location.protocol + "//" + window.location.hostname + ':8000/';
})(this);
