export const environment = {
  production: true,
  apiRoute: window['env']['baseUrl'] || window.location.protocol + "//" + window.location.hostname + ':8000/'
};
